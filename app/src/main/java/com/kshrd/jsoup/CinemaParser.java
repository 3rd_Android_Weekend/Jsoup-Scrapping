package com.kshrd.jsoup;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pirang on 8/6/17.
 */

public class CinemaParser extends AsyncTask<Void, Void, List<Movie>> {

    private static final String BASE_URL = "https://www.legend.com.kh";
    private static final String URL = BASE_URL + "/Browsing/Movies/ComingSoon";
    private MovieCallback movieCallback;

    public CinemaParser(MovieCallback movieCallback) {
        this.movieCallback = movieCallback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        movieCallback.onPreExecute();
    }

    @Override
    protected List<Movie> doInBackground(Void... voids) {

        List<Movie> movieList = new ArrayList<>();
        try {
            Document document = Jsoup.connect(URL).get();
            Elements elements = document.select("article#movies-list div.list-item.movie");


            for (Element element: elements){
                String title = element.select("div.item-details h3.item-title").get(0).text();
                String thumbnail = element.select("div.image-outer img").get(0).attr("src");
                thumbnail = "https://" + thumbnail.substring(thumbnail.indexOf("www"));
                String date = element.select("p.movie-opening-date").get(0).text();
                String detail = BASE_URL + element.select("div.title-wrapper a").get(0).attr("href");
                String trailer = null;
                Elements trailerElements = element.select("div.alt-action a.play");
                if (!trailerElements.isEmpty()){
                    Element e = trailerElements.get(0);
                    trailer = e.attr("href");
                }

                Movie movie = new Movie(title, thumbnail, date, detail, trailer);
                movieList.add(movie);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return movieList;
    }

    @Override
    protected void onPostExecute(List<Movie> movies) {
        super.onPostExecute(movies);
        movieCallback.onPostExecute(movies);
    }
}
